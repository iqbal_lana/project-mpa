import IconProfile from './Profile.svg';
import IconProfileActive from './ProfileActive';
import IconHome from './home.svg';
import IconHomeActive from './homeActive.svg';
import IconScan from './Scan.svg';
import IconScanActive from './ScanActive.svg';
import IconTopup from './Topup.svg';
import IconGetPoint from './getPoint.svg';
import IconPLN from './PLN.svg';
import IconPaket from './Paket.svg';
import IconPulsa from './Pulsa.svg';
import IconLainnya from './Lainnya.svg';
import IconBPJS from './BPJS.svg';
import IconPasca from './Pasca.svg';
import IconDeals from './Deals.svg';
import IconDealsActive from './DealsActive.svg';
import IconFinance from'./Finance.svg';
import IconFinanceActive from './FinanceActive.svg';
import IconLingkungan from './Lingkungan.svg';
import IconTV from './TV.svg';
import IconTransfer from './Transfer.svg';
import IconPromo from './Promo.svg';

export {
  IconPromo,
  IconProfile,
  IconProfileActive,
  IconTransfer,
  IconTV,
  IconLingkungan,
  IconDeals,
  IconDealsActive,
  IconFinance,
  IconFinanceActive,
  IconHome,
  IconHomeActive,
  IconScan,
  IconScanActive,
  IconTopup,
  IconGetPoint,
  IconPLN,
  IconPaket,
  IconPulsa,
  IconLainnya,
  IconBPJS,
  IconPasca,
};
