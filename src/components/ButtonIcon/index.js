import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {
  IconTopup,
  IconGetPoint,
  IconPasca,
  IconPulsa,
  IconPaket,
  IconBPJS,
  IconLainnya,
  IconLingkungan,
  IconTV,
  IconPLN,
  IconTransfer,
} from '../../assets';
import {WARNA_SEKUNDER} from '../../utils/constant';

const ButtonIcon = ({title, type}) => {
  const Icon = () => {
    if (title === 'Top up') return <IconTopup />;

    if (title === 'Transfer') return <IconTransfer />;

    if (title === 'History') return <IconGetPoint />;

    if (title === 'PLN') return <IconPLN />;

    if (title === 'Pulsa') return <IconPulsa />;

    if (title === 'Paket Data') return <IconPaket />;

    if (title === 'Pasca bayar') return <IconPasca />;

    if (title === 'BPJS') return <IconBPJS />;

    if (title === 'TV') return <IconTV />;

    if (title === 'Lingkungan') return <IconLingkungan />;

    if (title === 'Lainnya') return <IconLainnya />;
    

    return <IconTopup />;
  };

  return (
    <TouchableOpacity style={styles.container(type)}>
      <View style={styles.button(type)}>
        <Icon />
      </View>
      <Text style={styles.text(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default ButtonIcon;

const styles = StyleSheet.create({
  container: (type) => ({
      marginBottom : type === "layanan" ? 13 : 0,
      marginRight : type === "layanan" ? 7 : 1
      
  }), 
  button: (type) => ({
    backgroundColor: WARNA_SEKUNDER,
    padding: type === 'layanan' ? 10 : 15,
    borderRadius: 10,
  }),
  text: (type) => ({
    fontSize: type === 'layanan' ? 14 : 10,
    fontFamily:type === 'layanan' ? 'TitilliumWeb-Light' : 'TitilliumWeb-Regular',
    textAlign: 'center',
    
  }),

});
