import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {WARNA_UTAMA} from '../../utils/constant';
import ButtonIcon from '../ButtonIcon';
import Gap from '../Gap';

const Saldo = () => {
  return (
    <View style={styles.container}>
      <View style={styles.informasiSaldo}>
        <View style={styles.text}>
          <Text style={styles.labelSaldo}></Text>
          <Text style={styles.valueSaldo}></Text>
        </View>
        <View style={styles.text}>
          <Text style={styles.labelPoint}></Text>
          <Text style={styles.valuePoint}></Text>
        </View>
      </View>
      <View style={styles.buttonAksi}>
        <ButtonIcon title="Top Up" />
        <Gap width={50} />
        <ButtonIcon title="Transfer" />
        <Gap width={50} />
        <ButtonIcon title="History" />
      </View>
    </View>
  );
};

export default Saldo;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 17,
    marginHorizontal: 30,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 8,
    marginTop: -windowHeight * 0.07,
    flexDirection: 'row-reverse',
  },
  buttonAksi: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
